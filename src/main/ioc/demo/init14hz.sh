#!/bin/bash

caput SCANNINGTR:MSGS 	 OFF
sleep 1
caput SCANNINGTR:MSGS 	 INIT

# Configure event 5 on sequence
caput SCANNINGTR:SEQ1 5
caput SCANNINGTR:SEQ1-NITER 1000000

# Turn timing receiver on - sequencer will automatically be turned on 
caput SCANNINGTR:MSGS         ON

sleep 1

caput -S SCANNINGTR:SEQ1-MSGS START
