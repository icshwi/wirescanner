#!/bin/bash

caput SCANNINGTR:MSGS 	 INIT

# Configure pulse width on trigger line output 1
caput SCANNINGTR:TL3-WIDTH   5300

# Configure event 5 on sequence
caput SCANNINGTR:SEQ1 5
caput SCANNINGTR:SEQ1-NITER 1000000

# Turn timing receiver on - sequencer will automatically be turned on 
caput SCANNINGTR:MSGS         ON

sleep 1

caput -S SCANNINGTR:SEQ1-MSGS START

# Initialize Positioners in scan

caput SCANNING:WIRE1.P4SP -1
caput SCANNING:WIRE1.P4EP 1

caput SCANNING:WIRE1.P2SP 1
caput SCANNING:WIRE1.P2EP 1

caput SCANNING:WIRE1.NPTS 101
