#!../../bin/linux-x86_64/wirescanner

## You may have to change scanning-sample-wire to something else
## everywhere it appears in this file

< envPaths
epicsEnvSet "EPICS_DB_INCLUDE_PATH" ".:$(SCANNING)/db"

epicsEnvSet "STREAM_PROTOCOL_PATH" "$(CODAC_ROOT)/protocol:./../protocol"

# PMAC common macros

epicsEnvSet("NUM_MOTORS", 1       )
epicsEnvSet("NUM_AXES",   2       )   # This has to be NUM_MOTORS + 1
epicsEnvSet("PMACPREFIX", "PMAC"  )
epicsEnvSet("PMACPORT",   "pmacLV")
epicsEnvSet("MOTOR_PORT", "pmac1" )

# Motor Macros

epicsEnvSet("EGU",       "mm"   )
epicsEnvSet("DIR",       "Pos"  )
epicsEnvSet("MRES",      "0.001")
epicsEnvSet("PREC",      "4"    )
epicsEnvSet("DHLM",      "10"   )
epicsEnvSet("DLLM",      "-10"  )
epicsEnvSet("VELO",      "1"    )
epicsEnvSet("VBAS",      "0.1"  )
epicsEnvSet("VMAX",      "2"    )
epicsEnvSet("ACCL",      "0.1"  )
epicsEnvSet("BDST",      "0"    )
epicsEnvSet("INIT",      ""     )
epicsEnvSet("OFF",       "0"    )

epicsEnvSet("MOTOR_NAME1",  "MTR1")
epicsEnvSet("AXIS_NO1",     "1")

# Motor Status macros

epicsEnvSet("SCAN",         "1 second")
epicsEnvSet("OVERHEAT1",    "MAJOR"   )
epicsEnvSet("SWITCH_OFF1",  "MINOR"   )
epicsEnvSet("OVERHEAT2",    "0"       )
epicsEnvSet("SWITCH_OFF2",  "0"       )
epicsEnvSet("MOTOR_ERROR1", "0"       )
epicsEnvSet("MOTOR_ERROR2", "0"       )
epicsEnvSet("MOTOR_ERROR3", "0"       )
epicsEnvSet("MOTOR_ERROR4", "0"       )

# Timing receiver Macros
epicsEnvSet("TRPREFIX", "SCANNINGTR")
epicsEnvSet("TRPORT", "TRPIPE1"         )
epicsEnvSet("TIMEVENT", "5"             )

#DAQ macros
epicsEnvSet("DAQPREFIX", "SCANNINGDAQ"      )
epicsEnvSet("DAQ_ASYN_PORT", "SCANNINGDAQ"  )
epicsEnvSet("DAQBUFSIZE", "100"         )

#Scanning macros
epicsEnvSet("SCANPREFIX", "SCANNING"    )
epicsEnvSet("SCAN1", "WIRE1"            )
epicsEnvSet("SCAN_POINTS", "1000"       )

# PSCPT macros
epicsEnvSet("PSCPT1", "PSCPT1"            )
epicsEnvSet("PSCPT_EV", "99"            )

cd ${TOP}
#############################################
## Register all support components         ##
#############################################
dbLoadDatabase "dbd/wirescanner.dbd"
wirescanner_registerRecordDeviceDriver pdbbase

# Connection to PMAC and setup for $(NUM_MOTORS) motors
pmacAsynIPConfigure("$(PMACPORT)", "10.4.3.202:1025")
pmacAsynMotorCreate("$(PMACPORT)", 0, 0, $(NUM_MOTORS))

# Number of axes
drvAsynMotorConfigure("$(MOTOR_PORT)", "pmacAsynMotor", 0, $(NUM_AXES))

# Create timing receiver
ndsCreateDevice "ndsTr", "$(TRPORT)", "FILE=/dev/era3, HBEN=1, TG_SIMULATOR=1"
# Create DAQ card
ndsCreateDevice "pci9116", "$(DAQ_ASYN_PORT)", "CardNumber=0"


## Load record instances
#Load timing receiver records

dbLoadRecords "$(TR)/db/tr.db", "PREFIX=$(TRPREFIX), ASYN_PORT=$(TRPORT)"

# Sequence channel for built in timing generator

dbLoadRecords "${TR}/db/tgSequenceChannel.template", "PREFIX=$(TRPREFIX), ASYN_PORT=$(TRPORT), SEQUENCE_ID=0"
dbLoadRecords "${TR}/db/tgSequenceChannel.template", "PREFIX=$(TRPREFIX), ASYN_PORT=$(TRPORT), SEQUENCE_ID=1"

#Trigger lines used for both Pmac and DAQ.

dbLoadRecords "$(TR)/db/trAction.template", "PREFIX=$(TRPREFIX), ASYN_PORT=$(TRPORT), TL=2, EVENT=$(TIMEVENT), DEFOPT=0"

dbLoadRecords "$(TR)/db/trAction.template", "PREFIX=$(TRPREFIX), ASYN_PORT=$(TRPORT), TL=3, EVENT=$(TIMEVENT), DEFOPT=0"


# Load Single motions records

dbLoadRecords("${EPICS_MODULES}/singlemotion/db/motor.template", "DEVICE=$(PMACPREFIX),MOTOR_NAME=$(MOTOR_NAME1),MOTOR_PORT=$(MOTOR_PORT),AXIS_NO=$(AXIS_NO1),EGU=$(EGU),DIR=$(DIR),MRES=$(MRES),PREC=$(PREC),DHLM=$(DHLM),DLLM=$(DLLM),VELO=$(VELO),VBAS=$(VBAS),VMAX=$(VMAX),ACCL=$(ACCL),BDST=$(BDST),INIT=$(INIT),OFF=$(OFF),FLNK=$(SCANPREFIX):$(SCAN1)-AFTER_MV_HOOK")

dbLoadRecords("${EPICS_MODULES}/singlemotion/db/motorStatus.template", "DEVICE=$(PMACPREFIX),MOTOR_NAME=$(MOTOR_NAME1),ASYN_PORT=$(PMACPORT),AXIS_NO=$(AXIS_NO1),SCAN=$(SCAN),EGU=$(EGU),PREC=$(PREC),OVERHEAT1=$(OVERHEAT1),SWITCH_OFF1=$(SWITCH_OFF1),OVERHEAT2=$(OVERHEAT2),SWITCH_OFF2=$(SWITCH_OFF2),MOTOR_ERROR1=$(MOTOR_ERROR1),MOTOR_ERROR2=$(MOTOR_ERROR2),MOTOR_ERROR3=$(MOTOR_ERROR3),MOTOR_ERROR4=$(MOTOR_ERROR4)")

dbLoadRecords("${EPICS_MODULES}/singlemotion/db/motorHoming.template", "DEVICE=$(PMACPREFIX),MOTOR_NAME=$(MOTOR_NAME1),ASYN_PORT=$(PMACPORT),AXIS_NO=$(AXIS_NO1),PREC=$(PREC),EGU=$(EGU)")

# Load PMAC status template

dbLoadRecords("${EPICS_MODULES}/pmacstatus/db/pmacStatus.template", "DEVICE=$(PMACPREFIX),PORT=$(PMACPORT)")

# Load DAQ records for interfacing pci9116 DAQ card (detector)
#

dbLoadRecords "$(EPICS_MODULES)/pci9116/db/pci9116.db", "PREFIX=$(DAQPREFIX), ASYN_PORT=$(DAQ_ASYN_PORT), AI_NELM=$(DAQBUFSIZE)"

# Load position capture database necessary for on the fly scan
#

dbLoadRecords("$(EPICS_MODULES)/pscpt/db/pscptInit.template", "DEVICE=$(SCANPREFIX), PORT=$(PMACPORT), EVENT=$(PSCPT_EV), BUFFER_SIZE=$(SCAN_POINTS), NUM_AXES=$(NUM_MOTORS)"
dbLoadRecords("$(EPICS_MODULES)/pscpt/db/pscptAxis.template", "DEVICE=$(SCANPREFIX), PSCPT=$(PSCPT1), AXIS=$(AXIS_NO1), PORT=$(PMACPORT), EVENT=$(PSCPT_EV), MAX_BUFFER_SIZE=$(SCAN_POINTS), STEP_SIZE=$(PMACPREFIX):$(MOTOR_NAME1)_.MRES, POS_FLNK=$(SCANPREFIX):$(SCAN1)-NORD_POS, ITERADDR=21298, PLCNUM=11"


#
# Wirescanner COMPONENT
#

dbLoadRecords "db/wireScanner.db", "DEVICE=$(SCANPREFIX), SCAN=$(SCAN1), MTRREC=$(PMACPREFIX):$(MOTOR_NAME1)_, MAX_POINTS=$(SCAN_POINTS), DAQNDSPREFIX=$(DAQPREFIX), DAQBUFSIZE=$(DAQBUFSIZE), DET1WF=AI0, DET2WF=AI1, DET3WF=AI2, DET4WF=AI3, DET5WF=AI4, TR_TL1=$(TRPREFIX):ACT-TL3-EV$(TIMEVENT),  TR_TL2=$(TRPREFIX):ACT-TL2-EV$(TIMEVENT), TR_TLTSEL=$(TRPREFIX):ACT-TL3-EV$(TIMEVENT)-RBV.TIME, POS_ARR_RBV_PV=$(SCANPREFIX):$(PSCPT1)-POS_ARRAY, POS_ARR_NORD_RBV_PV=$(SCANPREFIX):$(PSCPT1)-NORD_POS, POS_ARR_RBV_RDY_PV=$(SCANPREFIX):$(PSCPT1)-DONE CP, ENAB_POSARR_INTERFACE=$(SCANPREFIX):$(PSCPT1)-ENPSCPT, DIS_POSARR_INTERFACE=$(SCANPREFIX):$(PSCPT1)-DISPSCPT"


cd ${TOP}/iocBoot/${IOC}
iocInit

# SET INTERFACE FROM TR TO POSTION CAPTURE
# Set forward link of the tl1 event 5 record to timing trigger counter in PSCPT1 where 1 is because of axis 1 
dbpf $(TRPREFIX):ACT-TL3-EV$(TIMEVENT)-RBV.FLNK $(SCANPREFIX):PSCPT1-TIM_TRIG_CNT 

# SET INTERFACE FROM PCI9116 TO WIRESCANNER
# Forward link wf record to Detector guard
dbpf $(DAQPREFIX):AI0.FLNK $(SCANPREFIX):DET1-GUARD
dbpf $(DAQPREFIX):AI1.FLNK $(SCANPREFIX):DET2-GUARD
dbpf $(DAQPREFIX):AI2.FLNK $(SCANPREFIX):DET3-GUARD
dbpf $(DAQPREFIX):AI3.FLNK $(SCANPREFIX):DET4-GUARD
dbpf $(DAQPREFIX):AI4.FLNK $(SCANPREFIX):DET5-GUARD
# Set tsel on WF record to action record on TR
dbpf $(DAQPREFIX):AI0.TSEL $(TRPREFIX):ACT-TL3-EV$(TIMEVENT)-RBV.TIME

# INITIALIZE PCI9116
# Set Analog in to differential measuring
dbpf $(DAQPREFIX):AI-DIFF 1
# Enable the analog inputs we are using
dbpf $(DAQPREFIX):AI0-ENBL 1
dbpf $(DAQPREFIX):AI1-ENBL 1
dbpf $(DAQPREFIX):AI2-ENBL 1
dbpf $(DAQPREFIX):AI3-ENBL 1
dbpf $(DAQPREFIX):AI4-ENBL 1
# Set sampling frequency
dbpf $(DAQPREFIX):AI-CLKF 8000
# Set pci9116 Analog input to retrigger 1, which is used in default step mode
dbpf $(DAQPREFIX):AI-TRGR 1


# Homing procedure sequence program
seq motorHoming "DEVICE=$(PMACPREFIX),MOTOR_NAME=$(MOTOR_NAME1)"
